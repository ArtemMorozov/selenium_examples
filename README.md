# Selenium WebDriver Java Examples
### Description
Framework provides automated tests for eCommerce platforms as BestBuy, Blurb and Yahoo Finance. 
Tests are WIP.

For jar version control used Maven. Tests based on JUnit.

#### Expected when done:
- to have all web elements placed in Page Factory classes;
- to integrate email reports;
- to implement more Modules/helpers for reusable methods;
- to extend coverage for test cases.

Made by <a href="https://www.linkedin.com/in/morozovartem/">Artem Morozov</a>.