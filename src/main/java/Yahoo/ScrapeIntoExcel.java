package Yahoo;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by @author Artem Morozov on 6/21/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class ScrapeIntoExcel {
    private String date;
    private String open;
    private String high;
    private String low;
    private String close;
    private String adjClose;
    private String volume;

    public ScrapeIntoExcel() {
    }

    public ScrapeIntoExcel(String date, String open, String high, String low, String close, String adjClose, String volume) {
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.adjClose = adjClose;
        this.volume = volume;
    }

    public String getDate() {
        return date;
    }

    public String getOpen() {
        return open;
    }

    public String getHigh() {
        return high;
    }

    public String getLow() {
        return low;
    }

    public String getClose() {
        return close;
    }

    public String getAdjClose() {
        return adjClose;
    }

    public String getVolume() {
        return volume;
    }

    public void writeExcel(List<ScrapeIntoExcel> list, String outputFile) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Historical prices of Spotify");
        int rowCount = 0;

        for (ScrapeIntoExcel scrape : list) {
            CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
            Font font = sheet.getWorkbook().createFont();
            font.setBold(true);
            cellStyle.setFont(font);

            Row row = sheet.createRow(0);
            row.createCell(0).setCellStyle(cellStyle);
            row.getCell(0).setCellValue("Date");

            row.createCell(1).setCellStyle(cellStyle);
            row.getCell(1).setCellValue("Open");

            row.createCell(2).setCellStyle(cellStyle);
            row.getCell(2).setCellValue("High");

            row.createCell(3).setCellStyle(cellStyle);
            row.getCell(3).setCellValue("Low");

            row.createCell(4).setCellStyle(cellStyle);
            row.getCell(4).setCellValue("Close");

            row.createCell(5).setCellStyle(cellStyle);
            row.getCell(5).setCellValue("AdjClose");

            row.createCell(6).setCellStyle(cellStyle);
            row.getCell(6).setCellValue("Volume");

            row = sheet.createRow(++rowCount);
            writeCells(scrape, row);
        }

        try (FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.dir") + outputFile)) {
            workbook.write(outputStream);
        }
    }

    private void writeCells(ScrapeIntoExcel scrape, Row row) {
        Cell cell = row.createCell(0);
        cell.setCellValue(scrape.getDate());

        cell = row.createCell(1);
        cell.setCellValue(scrape.getOpen());

        cell = row.createCell(2);
        cell.setCellValue(scrape.getHigh());

        cell = row.createCell(3);
        cell.setCellValue(scrape.getLow());

        cell = row.createCell(4);
        cell.setCellValue(scrape.getClose());

        cell = row.createCell(5);
        cell.setCellValue(scrape.getAdjClose());

        cell = row.createCell(6);
        cell.setCellValue(scrape.getVolume());
    }
} // end of scrapeIntoExcel