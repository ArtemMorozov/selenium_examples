package Yahoo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by @author Artem Morozov on 6/21/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Finance_PageFactory {
    WebDriver driver;

    public Finance_PageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@id='yfin-usr-qry']")
    WebElement searchField;

    @FindBy(id = "header-desktop-search-button")
    WebElement searchButton;

    @FindBy(xpath = "//ul/li[@title='Spotify Technology S.A.'][1]")
    WebElement dropdownMenu;

    @FindBy(xpath = "//div/span[@data-reactid='50']")
    WebElement priceAtTitle;

    @FindBy(xpath = "//div/span[@data-reactid='51']")
    WebElement resultOfTradingDayAtTitle;

    //TODO: make all tabs elements
    @FindBy(xpath = "//a/span[contains(text(), 'Historical Data')]")
    WebElement tabs_historicalData;

    @FindBy(xpath = "//div[@class='D(ib) Py(6px) Mend(40px) Mend(10px)--tab768 smartphone_Mend(0px)']")
    WebElement timePeriodHeader;

    @FindBy(xpath = "//div[@class='Pos(r) D(ib) C($linkColor) O(n):f Cur(p)']")
    WebElement dateDropdown;

    @FindBy(xpath = "//div[@data-test='date-picker-menu']")
    WebElement datePickerMenu;

    //TODO: make all buttons elements
    @FindBy(xpath = "//button[@data-value='3_M']")
    WebElement threeMonthsButton;

    @FindBy(xpath = "//button[@class=' Bgc($linkColor) Bdrs(3px) Px(20px) Miw(100px) Whs(nw) Fz(s) Fw(500) C(white) Bgc($linkActiveColor):h Bd(0) D(ib) Cur(p) Td(n)  Py(9px) Fl(end)']")
    WebElement applyButton;

    @FindBy(xpath = "//table[@data-test='historical-prices']/tbody")
    WebElement tableBody;

    @FindBy(xpath = "//*[@class='BdT Bdc($seperatorColor) C($tertiaryColor) H(36px)']")
    WebElement tableFooter;
} // end of Finance_PageFactory