package Yahoo;

import Modules.Modules;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Created by @author Artem Morozov on 6/21/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Finance {
    private WebDriver driver;
    private Finance_PageFactory pf;
    private ExtentReports reports;
    private ExtentTest logReport;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        pf = new Finance_PageFactory(driver);
        reports = new ExtentReports(System.getProperty("user.dir") + "//src/main/Reports/Yahoo/TestReport.html");
        logReport = reports.startTest("Automated test of Yahoo Finance experience");
    }

    @Test
    public void testYahooFinance() throws Exception {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Robot robot = new Robot();
        Modules modules = new Modules();
        Actions actions = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty("webdriver.chrome.driver", "/Users/morozov_artem/chromedriver");

        logReport.log(LogStatus.INFO, "Test started.");
        driver.get("https://finance.yahoo.com/");
        Thread.sleep(2000);

        //searching for Spotify stock
        wait.until(ExpectedConditions.elementToBeClickable(pf.searchField));
        pf.searchField.sendKeys("spotify");
        Thread.sleep(1000);
        modules.isDisplayed(pf.dropdownMenu, "pf.dropdownMenu", logReport, driver);
        pf.searchButton.click();

        //verifying url and title: title is changing after importing the price and % from info header
        String priceAtTitle = pf.priceAtTitle.getText();
        String priceAndPercentTitle = pf.resultOfTradingDayAtTitle.getText();
        if (driver.getCurrentUrl().contains("SPOT")) {
            modules.printAndLog_PassResult( logReport, "URL for Spotify page is correct.");
            try {
                assertEquals("Spotify Technology S.A. (SPOT) Stock Price, Quote, History & News - Yahoo Finance",
                        driver.getTitle());
                modules.printAndLog_PassResult(logReport, "Title for Spotify page is correct.");
            } catch (ComparisonFailure e) {
                StringBuilder sb = new StringBuilder();
                priceAndPercentTitle = priceAndPercentTitle.replace("(", "")
                        .replace(")", "").replace("+", "");
                sb.append("SPOT ").append(priceAtTitle).append(" ").append(priceAndPercentTitle)
                        .append(" : Spotify Technology S.A. - Yahoo Finance");
                Thread.sleep(9000); // title has to import from info header
                assertEquals(sb.toString(), driver.getTitle());
                modules.printAndLog_PassResult(logReport, "Updated title for Spotify page is correct.");
            }
        } else {
            modules.printAndLog_FailResult(logReport, "Failed to get to Spotify page.", driver);
        }

        pf.tabs_historicalData.click();
        modules.isDisplayed(pf.timePeriodHeader, "pf.timePeriodHeader", logReport, driver);
        modules.printAndLog_PassResult(logReport, "Opened Historical Data tab.");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        pf.dateDropdown.click();
        if (pf.datePickerMenu.isDisplayed()) {
            pf.threeMonthsButton.click();
            pf.applyButton.click();
            Thread.sleep(2000);
            //scrolling down to table footer to load all remaining rows
            jse.executeScript("arguments[0].scrollIntoView(true);", pf.tableFooter);
            modules.isDisplayed(pf.tableFooter, "pf.tableFooter", logReport, driver);

            int count = driver.findElements(By.xpath("//table[@data-test='historical-prices']/tbody/tr")).size();
            String startDate = "";
            ScrapeIntoExcel scrape = new ScrapeIntoExcel();
            List<ScrapeIntoExcel> list = new ArrayList<>();
            List<Double> closePrice = new ArrayList<>();
            List<Double> openPrice = new ArrayList<>();

            for (int i = 1; i <= count; i++) {
                String date = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                + i + "]/td[1]/span")).getText();
                String open = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                        + i + "]/td[2]/span")).getText();
                String high = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                        + i + "]/td[3]/span")).getText();
                String low = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                        + i + "]/td[4]/span")).getText();
                String close = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                        + i + "]/td[5]/span")).getText();
                String adjClose = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                        + i + "]/td[6]/span")).getText();
                String volume = driver.findElement(By.xpath("//table[@data-test='historical-prices']/tbody/tr["
                        + i + "]/td[7]/span")).getText();
                scrape = new ScrapeIntoExcel(date, open, high, low, close, adjClose, volume);
                list.add(scrape);
                closePrice.add(Double.parseDouble(close));
                openPrice.add(Double.parseDouble(open));
                if (i == count) startDate = date;
            }
            scrape.writeExcel(list, "//src/main/java/Yahoo/Scraping Info.xls");
            modules.printAndLog_PassResult(logReport, "Scraped data and wrote it in .xls table successfully.");

            double max = closePrice.get(0), min = openPrice.get(0), currentMax = 0, currentMin = 0;
            for (int i = 0; i < closePrice.size()-1; i++) {
                currentMax = Math.max(closePrice.get(i), closePrice.get(i+1));
                currentMin = Math.min(openPrice.get(i), openPrice.get(i+1));
                max = Math.max(max, currentMax);
                min = Math.min(min, currentMin);
            }
            double profit = (max * 1000) - (min * 1000);
            System.out.println("You would have $" + profit + " profit by now if you would purchase 1000 Spotify stocks at "
                    + startDate + ". Buying price: $" + min + " and selling price: $" + max + ".");
        } else {
            modules.printAndLog_FailResult(logReport, "Failed to scrape Historical prices data.", driver);
        }
    }

    @After
    public void cleanup() {
        reports.endTest(logReport);
        reports.flush();
        driver.quit();
    }

    //https://github.com/VincentTatan/ValueInvesting    |   https://towardsdatascience.com/in-12-minutes-stocks-analysis-with-pandas-and-scikit-learn-a8d8a7b50ee7
    //https://www.investopedia.com/ask/answers/032515/how-can-i-calculate-value-stock-gordon-grown-model-using-excel.asp
    // charts visualization https://www.roytuts.com/how-to-generate-line-chart-in-excel-using-apache-poi/
} // end of Finance