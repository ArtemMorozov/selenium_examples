# Selenium WebDriver Java Example of Yahoo Finance 
### Description
Framework provides automated test for Yahoo Finance platform. 
Test purpose is to show my knowledge in web automation as well as some Java skills used in this project.

For jar version control used Maven. Tests based on JUnit.

### Video example:
[![](http://img.youtube.com/vi/W2TrK6xlofg/0.jpg)](http://www.youtube.com/watch?v=W2TrK6xlofg "")


#### Expected when done:
- to have all web elements placed in Page Factory classes;
- to integrate email reports;
- to implement more Modules.Modules/helpers for reusable methods;
- to extend coverage for test cases.

Made by <a href="https://www.linkedin.com/in/morozovartem/">Artem Morozov</a>