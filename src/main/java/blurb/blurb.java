package blurb;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Artem Morozov on 1/30/20, morozov383@gmail.com
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class blurb {
    private WebDriver driver;

    @Before
    public void setUp () {
        driver = new ChromeDriver();
        driver.manage().deleteAllCookies();
        System.setProperty("webdriver.chrome.driver", "/Users/morozov_artem/chromedriver");
    }

    //TODO: create page factory
    //TODO: expend scope of tests
    //TODO: make a module for popup
    @Test
    public void test() throws Exception {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Actions actions = new Actions(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.get("https://www.blurb.com");
        WebElement closePopUp = driver.findElement(By.xpath("//*[@id='exit-modal-signup-and-save']/div/button"));
        try {
            if (closePopUp.isDisplayed()) {
                jse.executeScript("arguments[0].click();", closePopUp);
            }
        } catch (NoSuchElementException e) {
            closePopUp.click(); //may not work this way, find workaround
            System.out.println("Error, can not close popup: ");
            e.getStackTrace();
        }
        List<WebElement> allLinks = driver.findElements(By.tagName("a"));
        //Traversing through the list and printing its text along with link address
        for (WebElement link : allLinks) {
            System.out.println(link.getText() + " - " + link.getAttribute("href"));
        }
        WebElement okayCookies = driver.findElement(By.xpath("//button[@class='btn btn--border-white banner-modal__btn js-banner-modal-btn-close']"));
        okayCookies.click();
        WebElement bannerBottom = driver.findElement(By.xpath("//div[@class='sticky-banner']"));
        WebElement layflatLink = driver.findElement(By.xpath("//ul[@class='sticky-banner__list-items']/li[2]/a[1]"));
        try {
            if (bannerBottom.isDisplayed()) {
                System.out.println("Bottom banner is displayed.");
                jse.executeScript("arguments[0].click();", layflatLink);
                Thread.sleep(1000);
                if (driver.getTitle().equals("Layflat Photo Books & Albums | Blurb") &&
                        driver.getCurrentUrl().contains("layflat")) {
                    System.out.println("Layflat page is displayed.");
                } else {
                    System.out.println("Error redirecting to Layflat page.");
                }
            }
        } catch (NoSuchElementException e) {
            System.out.println("Bottom banner is not displayed: ");
            e.getStackTrace();
        }
        WebElement services = driver.findElement(By.xpath("//span[contains(text(), 'Services')]"));
        actions.moveToElement(services).perform();
        WebElement hireExpert_Header = driver.findElement(By.xpath("//li[@class='controls__current-subnav__item']/a[contains(text(), 'Hire an Expert')]"));
        try {
            if (hireExpert_Header.isDisplayed()) {
                jse.executeScript("arguments[0].click();", hireExpert_Header);
                Thread.sleep(1000);
                if (driver.getCurrentUrl().equals("https://www.blurb.com/dreamteam-collaborators") &&
                driver.getTitle().equals("Dream Team Collaborators - Hire an Expert | Blurb")) {
                    System.out.println("Hire an Expert page is displayed.");
                } else {
                    System.out.println("Error redirecting to 'Hire an Expert'.");
                }
            }
        } catch (NoSuchElementException e) {
            System.out.println("Dropdown header is not shown: ");
            e.getStackTrace();
        }

        WebElement logIn = driver.findElement(By.xpath("//li[@class='utility__list-item hidden-sm']/a[contains(text(), 'Log In')]"));
        logIn.click();
        Thread.sleep(1000);
        WebElement userName = driver.findElement(By.xpath("//*[@id='user_username']"));
        userName.sendKeys("Art");
        WebElement password = driver.findElement(By.xpath("//*[@id='user_password']"));
        password.sendKeys("123");
        WebElement text1 = driver.findElement(By.xpath("//*[@class='reg-and-login__head header-a2']"));
        Thread.sleep(1000);
        try {
            if (!text1.getText().equals("Super Log In")) {
                System.out.println("Verified text: " + text1.getText());
            }
        } catch (Exception e) {
            System.out.println("Error with 'Log In' text: ");
            e.getStackTrace();
        }
        WebElement loginButton = driver.findElement(By.id("sign-in-button"));
        loginButton.click();
    }

    @After
    public void close() {
       driver.quit();
    }
 } // end of blurb.blurb