package Modules;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Artem Morozov on 11/30/19, morozov383@gmail.com
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Modules {
    private WebDriver driver;

    public void JSClick(WebElement element) {
        try {
            if (element.isEnabled() && element.isDisplayed()) {
                System.out.println("Clicking on element '" + element.getText() + "' using JavascriptExecutor.");
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
            } else {
                System.out.println("Unable to click on element '" + element.getText() + "'.");
            }
        } catch (StaleElementReferenceException e) {
            System.out.println("Element '" + element.getText() + "' is not attached to the page document."
                    + Arrays.toString(e.getStackTrace()));
        } catch (NoSuchElementException e) {
            System.out.println("Element '" + element.getText() + "' was not found in DOM."
                    + Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            System.out.println("Unable to click on element '" + element.getText() + "' " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void isDisplayed(WebElement element, String message, ExtentTest et, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(element));
            element.isDisplayed();
            printAndLog_PassResult(et, "Element '" + message + "' is displayed and selected.");
        } catch (TimeoutException te) {
            printAndLog_FailResult(et, "Element '" + message + "' is not displayed", driver);
            System.out.print(": \n");
            te.printStackTrace();
            driver.quit();
        } catch (Exception e) {
            printAndLog_FailResult(et, "Element '" + message + "' is not displayed", driver);
            System.out.print(": \n");
            e.printStackTrace();
        }
    }

    public void moduleSelectCheckBox(WebElement element, String xpath, ExtentTest et, WebDriver driver) throws InterruptedException {
        if (!element.isSelected()) {
            element.click();
            Thread.sleep(2000);
            isDisplayed(driver.findElement(By.xpath(xpath)), "" + element, et, driver);
            printAndLog_PassResult(et, element.getText() + " option was selected.");
        } else {
            printAndLog_FailResult(et, "Error selecting " + element.getText() + " option.", driver);
        }
    }

    public void printAndLog_PassResult(ExtentTest logReport, String message) {
        logReport.log(LogStatus.PASS, message);
        System.out.println(message);
    }
    /*TODO: idea on how to combine pass and fail: make one boolean with if statement -> true if no exception for assertion
      and false otherwise. */
    public void printAndLog_FailResult(ExtentTest logReport, String message, WebDriver driver) {
        try {
            String img = getScreenshot(driver, message + "-");
            logReport.log(LogStatus.FAIL, message);
            logReport.log(LogStatus.ERROR, logReport.addScreenCapture(img));
            System.out.println(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getScreenshot(WebDriver driver, String screenshotName) throws Exception {
        String dateName = new SimpleDateFormat("yyyy_MM_dd_hh-mm-ss").format(new Date());
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String destination = System.getProperty("user.dir") + "//src/main/Screenshots/" + screenshotName + dateName + ".png";
        File finalDestination = new File(destination);
        FileUtils.copyFile(source, finalDestination);

        return destination;
    }
} // end of modules