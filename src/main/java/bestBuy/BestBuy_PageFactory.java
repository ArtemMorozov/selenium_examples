package bestBuy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by Artem Morozov on 1/21/20, morozov383@gmail.com
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class BestBuy_PageFactory {
    WebDriver driver;

    public BestBuy_PageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@class='widget widget-email-submission']")
    WebElement emailPopUp;

    @FindBy(xpath = "//button[@class='c-close-icon  c-modal-close-icon']")
    WebElement popupClose;

    //@FindBy(id = "gh-search-input")
    @FindBy(className = "search-input")
    WebElement search;

    @FindBy(className = "header-search-button") // xpath //button[@class='header-search-button']
    WebElement searchButton;

    @FindBy(xpath = "//span[@class='c-facet-text'][contains(text(), 'MacBooks')]")
    WebElement macBooksLink;

    @FindBy(xpath = "//span[@class='c-facet-text'][contains(text(), 'MacBook Pro')]")
    WebElement macBookPro;

    @FindBy(xpath = "//span[@class='c-facet-text'][contains(text(), 'MacBook Air')]")
    WebElement macBookAir;

    @FindBy(xpath = "//label[@for='systemmemoryram_facet-32-gigabytes']/div")
    WebElement ram_32GB;

    @FindBy(xpath = "//label[@for='laptopscreensizerange_facet-15\"-- 16\"']/div")
    WebElement screenOption15_16;

    @FindBy(xpath = "//label[@for='totalstoragecapacityrange_facet-480-- 1000GB']/div")
    WebElement storage_400_1000GB;

    @FindBy(xpath = "//label[@for='condition_facet-New']/div")
    WebElement conditionNew;

    @FindBy(xpath = "//div[@id='nested-parent_processormodelsv_facet-Intel-Core i9']/button/span[2]/i")
    WebElement intelCore_i9_dropDown;

    @FindBy(xpath = "//label[@for='child_processormodelsv_facet-Intel-9th Generation Core i9']/div")
    WebElement intelCore_i9_9thGen;

    @FindBy(id = "sort-by-select")
    WebElement sort;

    @FindBy(xpath = "//ol/li[4]//button[@class='btn btn-primary btn-sm btn-block btn-leading-ficon add-to-cart-button']")
    WebElement addToCart_3rd_choice;

    @FindBy(xpath = "//ol/li[4]//div[@class='priceView-hero-price priceView-customer-price']/span[1]")
    WebElement price_3rd_choice;

    @FindBy(xpath = "//span[contains(text(), 'Item added to cart')]")
    WebElement itemAddedToCart_text;

    @FindBy(xpath = "//a/span[contains(@class, 'cart-nav')]")
    WebElement dialogWindow_GoToCart_link;

    @FindBy(xpath = "//div[@class='dot']")
    WebElement cartItemsNumber;

    //TODO: test with multiple items in cart and see the difference in xpath
    @FindBy(xpath = "//div[@class='priceblock__primary-price']")
    WebElement cartPage_price;

    @FindBy(xpath = "//div[@class='listing-footer__pricing-value']")
    WebElement cartPage_productTotal;

    @FindBy(xpath = "//*[@class='listing-header__button']/button[@class='btn btn-lg btn-block btn-primary']")
    WebElement cartPage_CheckoutTop;

    @FindBy(xpath = "//button[@class='btn btn-secondary btn-lg cia-guest-content__continue js-cia-submit-button js-cia-guest-button']")
    WebElement continueAsGuest;

    @FindBy(xpath = "//div[@class='item-list__content item-list__price']/span[@class='cash-money']")
    WebElement checkoutPage_singleItemPrice;
} // end of PageFactory