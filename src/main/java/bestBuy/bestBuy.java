package bestBuy;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import Modules.Modules;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by Artem Morozov on 10/19/19, morozov383@gmail.com
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class bestBuy {
    private WebDriver driver;
    private Properties properties;
    private BestBuy_PageFactory pf;
    private ExtentReports reports;
    private ExtentTest logReport;

    @Before
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        properties = new Properties();
        FileInputStream fis;
        fis = new FileInputStream(System.getProperty("user.dir") +
                "//src/main/DATA.properties");
        properties.load(fis);
        pf = new BestBuy_PageFactory(driver);
        reports = new ExtentReports(System.getProperty("user.dir") + "//src/main/Reports/BestBuy/TestReport.html");
        logReport = reports.startTest("Automated MacBook Pro purchase");
    }

    //TODO: check on waits in the test
    @Test
    public void test_macbook_search() throws Exception {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Modules modules = new Modules();
        Actions actions = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS) ;
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty("webdriver.chrome.driver", "/Users/morozov_artem/chromedriver");

        driver.get("https://www.bestbuy.com/");
        Thread.sleep(3000);

        logReport.log(LogStatus.INFO, "Test started.");
        //closing dialog popup at homepage
        //TODO: interaction with email box
        try {
            if (pf.emailPopUp.isDisplayed()) {
                logReport.log(LogStatus.PASS, "Dialog window is displayed.");
                System.out.println("Dialog window is displayed");
            }
            pf.popupClose.click();
        } catch (NoSuchElementException e) {
            System.out.println("Dialog window can not be closed. Error: \n");
            e.printStackTrace();
        }

        Thread.sleep(1500);
        wait.until(ExpectedConditions.elementToBeClickable(pf.search));
        pf.search.sendKeys(properties.getProperty("Search_String"));
        Thread.sleep(2500);
        pf.searchButton.click();
        Thread.sleep(2500);
        wait.until(ExpectedConditions.visibilityOfElementLocated((
                By.xpath("//h1[@class='search-title'][contains(text(), '"
                        + properties.getProperty("Search_String") + "')]"))));
        assertEquals("Title is incorrect", driver.getTitle(),
                properties.getProperty("Search_String") + " - Best Buy");

        //========> filtering part begins <========
        pf.macBooksLink.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), ' in MacBooks')]")));
        modules.isDisplayed(pf.macBookPro, "pf.macBookPro", logReport, driver);
        //modules.isDisplayed(pf.macBookAir, logReport); // was removed from search by BestBuy
        pf.macBookPro.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), ' in MacBook Pro')]")));
        //TODO: make a module for if statement below
        actions.moveToElement(pf.ram_32GB).perform();
        if (!pf.ram_32GB.isSelected()) {
            pf.ram_32GB.click();
            Thread.sleep(2500);
            modules.isDisplayed(driver.findElement(By
                            .xpath("//li/button[@class='c-facet-button '][contains(text(), '32 gigabytes')]")),
                    "ram_32GB", logReport, driver);
        } else {
            System.out.println("Error selecting 32 gigabytes RAM.");
        }
        actions.moveToElement(pf.conditionNew).perform();
        if (!pf.conditionNew.isSelected()) {
            pf.conditionNew.click();
            Thread.sleep(2500);
            modules.isDisplayed(driver.findElement(By
                            .xpath("//li/button[@class='c-facet-button '][contains(text(), 'New')]")),
                    "conditionNew" , logReport, driver);
        } else {
            System.out.println("Error selecting New condition.");
        }
        actions.moveToElement(pf.screenOption15_16).perform();
        if (!pf.screenOption15_16.isSelected()) {
            pf.screenOption15_16.click();
            Thread.sleep(2500);
            modules.isDisplayed(driver.findElement(By
                            .xpath("//li/button[@class='c-facet-button '][contains(text(), '15\" - 16\"')]")),
                    "screenOption15_16", logReport, driver);
        } else {
            System.out.println("Error selecting 15\"-16\" screen.");
        }
        actions.moveToElement(pf.storage_400_1000GB).perform();
        if (!pf.storage_400_1000GB.isSelected()) {
            pf.storage_400_1000GB.click();
            Thread.sleep(2500);
            modules.isDisplayed(driver.findElement(By
                            .xpath("//li/button[@class='c-facet-button '][contains(text(), '480 - 1000GB')]")),
                    "storage_400_1000GB", logReport, driver);
        } else {
            System.out.println("Error selecting 400-1000GB storage selected.");
        }
        pf.intelCore_i9_dropDown.click();
        actions.moveToElement(pf.intelCore_i9_9thGen).perform();
        if (!pf.intelCore_i9_9thGen.isSelected()) {
            pf.intelCore_i9_9thGen.click();
            Thread.sleep(2500);
            modules.isDisplayed(driver.findElement(By.xpath("//li/button[@class='c-facet-button '][contains(text(), " +
                            "'Intel 9th Generation Core i9')]")), "intelCore_i9_9thGen", logReport, driver);
        } else {
            System.out.println("Error selecting Intel 9th Generation Core i9 storage selected.");
        }
        //Sorting dropdown menu
        Select sortDropDown = new Select(pf.sort);
        sortDropDown.selectByVisibleText("New Arrivals");
        //========> Adding to cart part <========\\
        //TODO: organize in a better way here
        int itemsAddedToCart = 0;
        String priceOfAddedItem = pf.price_3rd_choice.getText();
        jse.executeScript("arguments[0].click();", pf.addToCart_3rd_choice);
        try {
            modules.isDisplayed(pf.itemAddedToCart_text, "pf.itemAddedToCart_text", logReport, driver);
            itemsAddedToCart++;
            actions.moveToElement(pf.dialogWindow_GoToCart_link).click().perform();
            wait.until(ExpectedConditions.urlToBe("https://www.bestbuy.com/cart"));
            System.out.println("Cart page is displayed.");
            logReport.log(LogStatus.PASS, "Cart page is displayed.");
        } catch (Exception e) {
            System.out.println("Error adding item to cart.");
            logReport.log(LogStatus.FAIL, "Error adding item to cart.");
        }
        //TODO: add validation in other parts of the site such as items(n) tab, amount of items in the items list
        if (pf.cartItemsNumber.getText().equals(Integer.toString(itemsAddedToCart))) {
            System.out.println("Cart items number is correct.");
        } else {
            System.out.println("Cant get a number of items in cart!");
        }
        String cartPage_priceOfAddedItem = pf.cartPage_price.getText();
        String cartPage_productTotal = pf.cartPage_productTotal.getText();
        assertEquals("Price of added item is incorrect iat Cart page list", priceOfAddedItem, cartPage_priceOfAddedItem);
        logReport.log(LogStatus.PASS, "Price of added item is correct at Cart page.");
        assertEquals("Product total is incorrect.", priceOfAddedItem, cartPage_productTotal);
        logReport.log(LogStatus.PASS, "Product total at Cart page is correct for single added item.");
        Thread.sleep(7000);

        //========> Checkout part <========\\
        pf.cartPage_CheckoutTop.click();
        wait.until(ExpectedConditions.titleIs("Sign In to BestBuy.com"));
        assertEquals("Title is incorrect", driver.getTitle(), "Sign In to BestBuy.com");
        pf.continueAsGuest.click();
        String checkoutPage_priceOfAddedItem = pf.checkoutPage_singleItemPrice.getText();
        assertEquals("Product price of added item at Checkout page is incorrect.",
                checkoutPage_priceOfAddedItem, cartPage_priceOfAddedItem);
        logReport.log(LogStatus.PASS, "Product price at Checkout page is correct for single added item.");
    }

    @After
    public void cleanup() {
        reports.endTest(logReport);
        reports.flush();
        driver.quit();
        //TODO: set up email reporting for project.
    }
} // end of test