package honey;

import Modules.Modules;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.Before;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by @author Artem Morozov on 6/15/20, email: morozov383@gmail.com,
 * LinkedIn: https://www.linkedin.com/in/morozovartem/
 */
public class Honey {
    private WebDriver driver;
    private ExtentReports reports;
    private ExtentTest logReport;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        reports = new ExtentReports(System.getProperty("user.dir") + "//src/main/Reports/Honey/TestReport.html");
        logReport = reports.startTest("Automated test of Honey chrome extension experience");
    }

    public void testMacysAndHoneyInteraction() throws Exception {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Robot robot = new Robot();
        Modules modules = new Modules();
        Actions actions = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty("webdriver.chrome.driver", "/Users/morozov_artem/chromedriver");

        driver.get("https://www.macys.com/");
        Thread.sleep(3000);

        logReport.log(LogStatus.INFO, "Test started.");
    }
} // end of Honey